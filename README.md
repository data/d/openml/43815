# OpenML dataset: ECDC-daily-data-on-COVID19-geographic-distribution

https://www.openml.org/d/43815

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data on the geographic distribution of COVID-19 cases worldwide.
The dataset includes the reported number of diagnosed cases and deaths each day in every country in the world.
The main fields include date, number of reported cases, number of reported deaths, country.
The csv file is a direct transformation of the original xls provided by the European Centre for Disease Prevention and Control. ECMC is the provider of the data, the dataset is posted here to increase visibility and allow easier access to the Kaggle community.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43815) of an [OpenML dataset](https://www.openml.org/d/43815). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43815/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43815/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43815/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

